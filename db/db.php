<?php
    include("../config/configSQL.php");
    class Video {

        static function add($url, $id_user) {
            
            $bdd = connect();
            $request = $bdd->prepare("INSERT INTO Vidéos VALUES(NULL, :url, :id)");
            $request->execute(['url' => $url, 'id' => $id_user]);
        }

        static function delete($id) {

            $bdd = connect();
            $request = $bdd->prepare('DELETE FROM Vidéos WHERE id = :id');
            $request->execute(['id' => $id]);
        }

        static function play($url) {
            
            $bdd = connect();

        }
    }

    class Utilisateurs {

        static function user($pseudo, $password) {
            $bdd = connect();
            $request = $bdd->prepare("SELECT id,pseudo FROM Utilisateurs WHERE pseudo =:pseudo AND password =:password");
            $request->execute(['pseudo'=>$pseudo, 'password'=>$password]);
            $request = $request->fetch();
            return $request;
        }

        static function newuser($name, $pseudo, $password) {
            $bdd = connect();
            $request = $bdd->prepare("INSERT INTO Utilisateurs VALUES(NULL, :name, :pseudo, :password)");
            $request->execute(['name' => $name, 'pseudo'=>$pseudo, 'password'=>$password]);
        }
    }

?>