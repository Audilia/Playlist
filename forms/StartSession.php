<?php
session_start();

    include("../db/db.php");

    $pseudo = $_POST['pseudo'];
    $password = $_POST['password'];
    
    $log = Utilisateurs::user($pseudo, $password);

    if (empty($log)) {
        $_SESSION['Error'] = 'Login invalid';
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    else {
        $_SESSION["id_user"] = $log["id"];
        $_SESSION["pseudo"] = $log["pseudo"];
        header('Location: ../playlist.php' );
    }
    
    

?>