<?php
session_start();
// if (!isset($_SESSION["pseudo"])) {
//     die('tes pa konekté mek');
// }
$user_name = $_SESSION["pseudo"];
$user = isset($_SESSION['id_user']) ? $_SESSION['id_user'] : '';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/stylePlaylist.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <title>Playlist</title>
</head>
<body>
    <?php
        include("config/configSQL.php");
        $bdd = connect();


        $request = $bdd->prepare('SELECT * FROM Vidéos INNER JOIN Utilisateurs ON Utilisateurs.id = Vidéos.id_user WHERE Utilisateurs.id = :user');
        $request->execute(['user' => $user]);
        $video = $request->fetchAll();
        $videos = [];
    ?>

    <div class="playing">
        <div id="player"></div>
    </div>
    <button type="submit" class="logout" name="logout"><a href="user.php">Log Out</a></button>
    <h1>Playlist</h1>
    <div class="name">Hello <?php echo $user_name ?>!</div>
    <div class="bloc">
        <div class="bloc1">
            <ul>
                <?php foreach ($video as $value) { 
                    $videos[] = $value['URL'] ?>
                        
                    <form>
                        <input type="hidden" name="id" value="<?php echo $value['id'] ?>" />
                    </form>
                        
                    <li>
                        <div class="url">
                            <img name="img" src= https://img.youtube.com/vi/<?php echo $value['URL']?>/default.jpg >
                            <?php 
                                $array = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/videos?part=id,snippet&id='.$value['URL'].'&key=AIzaSyDhZQ6EwlhepToVDZEhk64bpB9ZdRyTwRA'));
                                $title = $array->items[0]->snippet->title;
                                echo $title;
                            ?>
                            <form action="forms/deleteVideo.php" method="POST" class="delete">
                                <input type="hidden" name="id" value="<?php echo $value[0] ?>" />
                                <button type="submit" class="dicon"><img src="assets/delete.png"></img></button>
                            </form>
                            <input type="hidden" name="id" value="<?php echo $value[0] ?>" />
                            <button type="submit" class="play" data-id = "<?php echo $value['URL'] ?>"><img src="assets/play.png"></img></button>
                            <button type="submit" class="pause"><img src="assets/pause.png"></img></button>
                        </div>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <div class="play">
            <form class="ajout"action="forms/addVideo.php" method="POST">
                <input type="text" name="url" placeholder="Enter valid URL like https://...">
                <button type="submit" class="add"><img src="assets/add.png"></img></button>
            </form>
        </div>
    </div>
    <script>
        // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;
        var playlist = <?php echo json_encode($videos) ?>;
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
            height: '600',
            width: '600',
            videoId: playlist[0],
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
            });
        }

        // 4. The API will call this function when the video player is ready.
        function toArray(nodelist) {
            return ([].slice.call(nodelist))
        }
        function onPlayerReady(event) {
            var play = document.querySelectorAll(".play");
            play.forEach(function(button) {
                button.addEventListener("click", function(event){
                    player.loadVideoById(this.dataset.id);
                    player.playVideo();
                });
            });
    
            var pause = document.querySelectorAll(".pause");
                pause.forEach(function(button) {
                button.addEventListener("click", function(){
                    player.pauseVideo();
                });
            });
        }

        // 5. The API calls this function when the player's state changes.
        var i = 0;
        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.PLAYING) {
            }
            if (event.data == YT.PlayerState.ENDED) {
                player.loadVideoById(playlist[++i]);
            }
        }

        function stopVideo() {
            player.stopVideo();
        }
        </script>
        

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="forms/Add.js"></script>
</body>
</html>