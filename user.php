<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/styleUser.css" type="text/css">
    <title>Session</title>
</head>
<body>
    <?php
        include("config/configSQL.php");
        $error = isset($_SESSION['Error']) ? $_SESSION['Error'] : '';
        $error1 = isset($_SESSION['Error1']) ? $_SESSION['Error1'] : '';
    ?>

    <form class="user" action="forms/StartSession.php" method="POST">
        <input type="text" name="pseudo" placeholder="Pseudo">
        <input type="password" name="password" placeholder="Password">
        <button type="submit">Connect</button></br>
        <div class="error"><?php echo $error; ?></div>
    </form>

    <form class="new_user" method="POST" action="forms/NewSession.php" >
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="pseudo" placeholder="Pseudo">
        <input type="password" name="password" placeholder="Password">
        <button type="submit">Register</button>
        <div class="error"><?php echo $error1; ?></div>
    </form>
</body>
</html>
<?php
session_destroy();
?>