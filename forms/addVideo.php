<?php
    include("../db/db.php");
    session_start();
    $url = $_POST['url'];
    $id_user = $_SESSION['id_user'];

    function check_substr($str){
        if (preg_match('/^http[s]?:\/\/www\.youtube\.com\/watch\?v=.*$/',$str)){
            preg_match('/^.*=([\w]*)&?.*$/',$str,$matches);
            return $matches[1];
        }
        else if (preg_match('/http[s]?:\/\/youtu.be\/.*$/',$str)){
            preg_match('/^.*be\/([\w]*)\?.*$/',$str,$matches);
            return $matches;
        }
        else{
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
     }
     $urlRetrieve = check_substr($url);

    Video::add($urlRetrieve, $id_user);

    header('Location: ' . $_SERVER['HTTP_REFERER']);

?>
